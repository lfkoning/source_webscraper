import sys
import requests
from bs4 import BeautifulSoup as bs


def get_html(url):
    """
    Retrieves the HTML source code for an URL and parses it using BeautifulSoup.
    
    Args:
        url     The URL of the web page as string
        
    Returns:
        A beautifulsoup object for the web page
    """
    # Request the page source
    try:
        resp = requests.get(url)
    # If an error occurs, raise an error message
    except Exception as e:
        raise RuntimeError('An error occured while fetching the page: %s!' % e)

    # Handle some of the most common HTTP errors
    if resp.status_code != requests.codes.ok:
        # Authentication issues
        if resp.status_code in (401, 403):
            raise RuntimeError('HTTP Error: You need authorization to access the page (HTTP 401/403)!')
        
        # Page not found
        elif resp.status_code == 404:
            raise RuntimeError('HTTP Error: The page was not found (HTTP 404)!')
        
        # Internal server error
        elif resp.status_code == 500:
            raise RuntimeError('HTTP Error: The server made an error (HTTP 500)!')
        
    # Parse the source using BeautifulSoup
    html = bs(resp.text, 'html.parser')

    # Return the HTML object
    return html


def main():
    """
    Main program; fetches the web page, parses the source code and retrieves some HTML elements.
    """
    # Set the URL for the webpage
    url  = 'http://www.lukaskoning.nl/scrape_test.html'
    
    # Get HTML source from the URL
    try:
        html = get_html(url)
    except RuntimeError as e:
        # Exit nicely if an error occured
        print(e)
        sys.exit(1)
        
    # Find the link to Google and print its text (<a id="google_link">)
    glink = html.find('a', id = 'google_link')
    if glink:
        print('Google link text: %s\n\n' % glink.get_text())

    # Find and print all links in the text (all <a class="inline_link"> elements)
    links = html.find_all('a', class_ = 'inline_link')
    for link in links:
        print('Text: %s\nLinks to: %s\n\n' % (link.get_text(), link.get('href')))

    # Find the unordered list called bullet_points
    ul = html.find('ul', id = 'bullet_points')

    # If the list is found...
    if ul:
        # Get all the items (<li> tags)
        li = ul.find_all('li')

        # Print the second item (with index 1)
        if len(li) > 1:
            print('List item text: %s\n\n' % li[1].get_text())

    # The example page also creates dynamic content with Javascript.
    # Because this content is not present in the HTML source code, we cannot scrape it.
    dyn = html.find('div', 'dynamic_content')
    if not dyn:	
        print('Could not find a div with id "dynamic_content".')
 

# Call main routine
if __name__ == '__main__':
    main()