#  Webscraping with Requests and BeautifulSoup


## Process

In this example, the source code of a web page is downloaded by the requests package. Once the source code is downloaded, it is parsed by the BeautifulSoup package. BeautifulSoup allows us to search the source code for certain HTML elements. The HTML elements can then be processed as we see fit, for example, we could store the text of an element or scrape additional hyperlinks.

Note that this kind of webscraping only allows us to fetch HTML elements present in the source code. Dynamically generated content will not be available for scraping (see the webdriver example for scraping dynamic content).


## Setting up

First we will need to install the requests package to request the web page. Typically you can install this package using pip:

```pip install requests```

If you have Windows and Anaconda installed, you can use the conda command instead:

```conda install requests```

If you cannot use either conda or pip, you can download the requests package from: https://github.com/kennethreitz/requests

Once you have unzipped the code, you can install with:

```python setup.py install```

Next you will need to install BeautifulSoup. With pip or Anaconda you can use:

```pip install beautifulsoup4```

```conda install beautifulsoup4```

Alternatively, you can download the package from: https://www.crummy.com/software/BeautifulSoup/bs4/download/4.0/


## Basic Example

The script in this repository provides a very basic example for implementing a web scraper. The code downloads and parses a web page from my own website and retrieves a couple of HTML elements. The comments inside the code will hopefully help you understand how the process works.

Obviously, the code is kept brief and simple to make it easier to understand. You could build on this example to create a more advanced web scraper yourself. Alternatively, you could look into the Scrapy package if you want a very complete webscraping package.


## Documentation

BeautifulSoup:
https://www.crummy.com/software/BeautifulSoup/bs4/doc/

Requests:
http://docs.python-requests.org/en/master/
